package sample.ReadWrite;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Write {
    public static void Ouputfile(TreeMap<String,String> outputlist,String path){
        try {
            FileOutputStream out = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(out,"UTF-8");
            BufferedWriter writer = new BufferedWriter(osw);
            for (Map.Entry<String,String> tree : outputlist.entrySet()){
                writer.write(tree.getKey() +"\t" +tree.getValue());
                writer.newLine();
            }
            writer.close();
            osw.close();
            out.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
