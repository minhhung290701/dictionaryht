package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import sample.ReadWrite.*;


import java.net.URL;
import java.util.*;

public class FavoriteController extends AddController implements Initializable {
    private TreeMap<String,String> myFavorite = Read.readFileDictionary("Favorite.txt");
    @FXML private ListView<String> favoriteEng;
    @FXML private ListView<String> favoriteViet;
    private ObservableList<String> word_list = FXCollections.observableArrayList();
    private ObservableList<String> mean_list = FXCollections.observableArrayList();



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (Map.Entry<String,String> favo : myFavorite.entrySet()){
            word_list.add(favo.getKey());
            mean_list.add(favo.getValue());
        }
        //decralation
        favoriteEng.setItems(word_list);
        favoriteViet.setItems(mean_list);
    }
    //back to dictionary
    public void BackToDictionary(MouseEvent event){
        super.BackToScene(event);
    }
    //delete word of favorite list

    public void DeleteWordOfFavorateList(MouseEvent mouseEvent){
        Alert infor = new Alert(Alert.AlertType.CONFIRMATION);
        infor.setContentText("Xóa khỏi danh sách yêu thích");
        infor.setTitle("Bạn chọn");
        infor.setHeaderText("Delete");
        infor.showAndWait();
        String Wd = favoriteEng.getSelectionModel().getSelectedItem();
        if (infor.getResult() == ButtonType.OK && Wd!= null){

            for (Map.Entry<String,String> hobbies : myFavorite.entrySet()){
                if (hobbies.getKey().equals(Wd)){
                    favoriteEng.getItems().remove(Wd);
                    favoriteViet.getItems().remove(hobbies.getValue());
                }
            }
            if (myFavorite.containsKey(Wd)){
                myFavorite.remove(Wd);
            }
            Write.Ouputfile(myFavorite,"Favorite.txt");
        }
        if (Wd == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.setContentText("Bạn chưa chọn từ để xóa");
            alert.show();
        }
    }
}
