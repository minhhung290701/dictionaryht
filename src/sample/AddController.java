package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.ReadWrite.Write;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class AddController implements Initializable {
    @FXML
    private TextField WordEa;
    @FXML
    private TextField WordVa;
    private TreeMap<String,String> listAdd = Controller.getMyList();


    public void submitWords(){
        String eng = WordEa.getText();
        String vie = WordVa.getText();
        if(!eng.isEmpty() && !vie.isEmpty()){
            if(listAdd.containsKey(eng)){
                Alert al = new Alert(Alert.AlertType.ERROR);
                al.setTitle("Error");
                al.setContentText("Từ đã có trong từ điển");
                al.show();
                WordEa.clear();
                WordVa.clear();
            }
            else {
                listAdd.put(eng, vie);
                Alert al = new Alert(Alert.AlertType.CONFIRMATION);
                al.setTitle("Complete");
                al.setContentText("Bạn đã thêm từ thành công");
                al.show();
                Write.Ouputfile(listAdd,"MyFile.txt");
                WordEa.clear();
                WordVa.clear();
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Bạn chưa nhập từ hoặc nghĩa");
            alert.show();
        }
    }



    public void BackToScene(MouseEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("sample.fxml"));
            Parent newSceneAdd = loader.load();
            Scene backToMenu = new Scene(newSceneAdd);
            Stage newWindow = (Stage)((Node)event.getSource()).getScene().getWindow();
            newWindow.setScene(backToMenu);
            newWindow.show();

        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }


    public void pressOnEnter(KeyEvent event){
        if(event.getCode() == KeyCode.ENTER){
            submitWords();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

}
