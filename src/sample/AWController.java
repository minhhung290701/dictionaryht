package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import sample.ReadWrite.*;


import java.net.URL;
import java.util.*;

public class AWController extends AddController implements Initializable {
    private TreeMap<String,String> AllW = Read.readFileDictionary("MyFile.txt");
    @FXML private ListView<String> AllEng;
    @FXML private ListView<String> AllViet;

    @FXML private TableView<Word> TabA;
    @FXML private TableColumn<Word, String> TA;
    @FXML private TableColumn<Word, String> TV;
    private ObservableList<Word> wordlist;

    private ObservableList<String> word_list = FXCollections.observableArrayList();
    private ObservableList<String> mean_list = FXCollections.observableArrayList();

    //back to dictionary
    public void BackToDictionary(MouseEvent event){
        super.BackToScene(event);
    }

    public void clickedOnListView(MouseEvent event) {
        try {
            for (Map.Entry<String, String> fx : AllW.entrySet()) {
                if (fx.getKey().equals(AllEng.getSelectionModel().getSelectedItem())) {
                    AllViet.getItems().clear();
                    AllViet.getItems().add(fx.getValue());
                }
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void keyHangleOnListView(KeyEvent event) {
        String eng = AllEng.getSelectionModel().getSelectedItem();
        for (Map.Entry<String,String> focus : AllW.entrySet()){
            if(focus.getKey().equals(eng)){
                AllViet.getItems().clear();
                AllViet.getItems().add(focus.getValue());
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (Map.Entry<String,String> allw : AllW.entrySet()){
            word_list.add(allw.getKey());
            mean_list.add(allw.getValue());
        }
        //TA.setCellValueFactory(new PropertyValueFactory<Word, String>("TiengAnh"));
        //TV.setCellValueFactory(new PropertyValueFactory<Word, String>("TiengViet"));
        //TabA.setItems(wordlist);
        TabA.setVisible(false);
        AllEng.setItems(word_list);
        AllViet.setItems(mean_list);
    }
}
