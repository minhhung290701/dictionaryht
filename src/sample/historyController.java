package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import sample.ReadWrite.*;


import java.net.URL;
import java.util.*;

public class historyController extends AddController implements Initializable {
    private TreeMap<String,String> myhistory = Read.readFileDictionary("history.txt");
    private TreeMap<String,String> Em = Read.readFileDictionary("abc.txt");
    @FXML private ListView<String> hisEng;
    @FXML private ListView<String> hisViet;
    private ObservableList<String> word_list = FXCollections.observableArrayList();
    private ObservableList<String> mean_list = FXCollections.observableArrayList();



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (Map.Entry<String,String> favo : myhistory.entrySet()){
            word_list.add(favo.getKey());
            mean_list.add(favo.getValue());
        }
        //decralation
        hisEng.setItems(word_list);
        hisViet.setItems(mean_list);
    }
    //back to dictionary
    public void BackToDictionary(MouseEvent event){
        super.BackToScene(event);
    }
    //delete word of favorite list

    public void DeleteWordOfHistory(MouseEvent mouseEvent){

        Alert infor = new Alert(Alert.AlertType.CONFIRMATION);
        infor.setContentText("Bạn chắc chắn muốn xóa lịch sử tra từ");
        infor.setTitle("Bạn chọn");
        infor.setHeaderText("Xóa");
        infor.showAndWait();
        if (infor.getResult() == ButtonType.OK) {
            hisEng.getItems().clear();
            hisViet.getItems().clear();
            Write.Ouputfile(Em,"history.txt");
        }
    }
}
