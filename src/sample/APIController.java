package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import sample.Translate.GoogleTranslate;
import sample.Audio.SoundGoogle;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class APIController extends AddController implements Initializable {
    @FXML private TextArea inputW;
    @FXML private Button SE;
    @FXML private ImageView SoE;
    @FXML private WebView webview;
    private WebEngine webEngine;
    @FXML private ComboBox<String> combobox;

    private Image loa=new Image("sample/images/loa.jpg");

    private ObservableList<String> language = FXCollections.observableArrayList("Anh - Việt","Việt - Anh","Việt - Nhật"
            ,"Nhật - Việt");
    public void goToBackDictionary(MouseEvent event){
        super.BackToScene(event);
    }


    public void Translate1(ActionEvent event) throws IOException{
        try{
            boolean isMyComboBoxEmpty = combobox.getSelectionModel().isEmpty();
            if (!isMyComboBoxEmpty){
                webEngine.loadContent("");
                if(combobox.getValue().equals("Việt - Anh")) {
                    String words = inputW.getText();
                    String mean = GoogleTranslate.translate("en",words);
                    String html = "<html>" + mean +"</html>";
                    webEngine.loadContent(html);
                }
                else if(combobox.getValue().equals("Anh - Việt")){
                    String words = inputW.getText();
                    String mean = GoogleTranslate.translate("vi",words);
                    String html = "<html>" + mean +"</html>";
                    webEngine.loadContent(html);

                }

                else if(combobox.getValue().equals("Việt - Nhật")){
                    String words = inputW.getText();
                    String mean = GoogleTranslate.translate("ja",words);
                    String html = "<html>" + mean +"</html>";
                    webEngine.loadContent(html);
                }
                else if(combobox.getValue().equals("Nhật - Việt")){
                    String words = inputW.getText();
                    String mean = GoogleTranslate.translate("vi",words);
                    String html = "<html>" + mean +"</html>";
                    webEngine.loadContent(html);
                }
            }
            else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setContentText("Bạn chưa chọn chế độ dịch");
                alert.show();
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void MousePressed(){
        boolean isMyComboBoxEmpty = combobox.getSelectionModel().isEmpty();
        if (!isMyComboBoxEmpty){
            if (combobox.getValue().equals("Việt - Anh")){
                SoundGoogle.speak(inputW.getText(),"vi");
            }
            else if (combobox.getValue().equals("Anh - Việt")){
                SoundGoogle.speak(inputW.getText(),"en");
            }
            else if(combobox.getValue().equals("Việt - Nhật")){
                SoundGoogle.speak(inputW.getText(),"vi");
            }
            else if(combobox.getValue().equals("Nhật - Việt")){
                SoundGoogle.speak(inputW.getText(),"ja");
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setContentText("Bạn chưa chọn chế độ dịch");
            alert.show();
        }
    }


    public void MousePressed2() throws IOException{
        String mean = "";
        String words = inputW.getText();
        boolean isMyComboBoxEmpty = combobox.getSelectionModel().isEmpty();
        if (!isMyComboBoxEmpty){
            if (combobox.getValue().equals("Việt - Anh")){
                mean = GoogleTranslate.translate("en",words);
                SoundGoogle.speak(mean,"en");
            }
            else if (combobox.getValue().equals("Anh - Việt")){
                mean = GoogleTranslate.translate("vi",words);
                SoundGoogle.speak(mean,"vi");
            }
            else if(combobox.getValue().equals("Việt - Nhật")){
                mean = GoogleTranslate.translate("ja",words);
                SoundGoogle.speak(mean,"ja");
            }
            else if(combobox.getValue().equals("Nhật - Việt")){
                mean = GoogleTranslate.translate("vi",words);
                SoundGoogle.speak(mean,"vi");
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setContentText("Bạn chưa chọn chế độ dịch");
            alert.show();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        webEngine = webview.getEngine();
        combobox.setItems(language);
    }
}
