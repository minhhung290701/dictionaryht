package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import sample.ReadWrite.Write;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class ModifyController extends AddController implements Initializable {
    @FXML
    private TextField WordEm;
    @FXML
    private TextField WordVm;
    private TreeMap<String,String> fixList = Controller.getMyList();

    public void backToDictionary(MouseEvent event){
        super.BackToScene(event);
    }

    public void submitFixWords(){
        String eng = WordEm.getText();
        String vie = WordVm.getText();
        if(!fixList.containsKey(eng)){
            Alert infor = new Alert(Alert.AlertType.ERROR);
            infor.setTitle("Error");
            infor.setContentText("Từ không có trong từ điển");
            infor.show();
            WordEm.clear();
            WordVm.clear();
        }
        else if(!eng.isEmpty() && !vie.isEmpty())
        {
            fixList.put(eng,vie);
            Alert tb = new Alert(Alert.AlertType.CONFIRMATION);
            tb.setTitle("Complete");
            tb.setContentText("Bạn đã sửa thành công");
            tb.show();
            Write.Ouputfile(fixList,"MyFile.txt");
            WordEm.clear();
            WordVm.clear();
        }
        else {
            Alert infor = new Alert(Alert.AlertType.ERROR);
            infor.setTitle("Error");
            infor.setContentText("Bạn chưa nhập từ");
            infor.show();
            WordEm.clear();
            WordVm.clear();
        }
    }

    public void clickEnter(KeyEvent keyEvent){
        if(keyEvent.getCode() == KeyCode.ENTER){
            submitFixWords();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
