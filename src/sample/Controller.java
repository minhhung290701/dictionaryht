package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;
import sample.Audio.SoundGoogle;
import sample.ReadWrite.*;
import java.net.URL;
import java.util.*;


public class Controller implements Initializable {
    @FXML
    private TextField input;
    @FXML
    Parent root;
    @FXML
    private ListView<String> listview;
    @FXML
    private ListView<String> meaning;
    @FXML
    WebView mean;
    @FXML
    ImageView soundV;
    @FXML
    ImageView soundE;
    @FXML
    ImageView star;

    @FXML
    private ListView<String> allview;
    private ObservableList<String> control_View = FXCollections.observableArrayList();
    private ObservableList<String> all_View = FXCollections.observableArrayList();


    private String meanw;

    private static TreeMap<String,String> favorite_list;
    private static TreeMap<String,String> history_list;
    private static TreeMap<String,String> myList;


    public static TreeMap<String, String> getMyList() {
        return myList;
    }

    public static void setMyList(TreeMap<String, String> myList) {
        Controller.myList = myList;
    }


    public void removeBackspace(KeyEvent keyEvent){
        if(keyEvent.getCode() == KeyCode.BACK_SPACE){
            listview.getItems().clear();
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            listview.requestFocus();
            listview.getSelectionModel().select(0);
            listview.getFocusModel().focus(0);
        }
    }


    public void search_Word(){
        try{
            String word = input.getText();
            WebEngine webEngine = mean.getEngine();
            if (word.isEmpty()) {
                Alert al = new Alert(Alert.AlertType.INFORMATION);
                al.setContentText("Hãy nhập từ để tìm kiếm");
                al.show();
                webEngine.loadContent("");
                meanw="Hãy nhập từ để tìm kiếm";
            }
            else{
                for (Map.Entry<String, String> m : myList.entrySet()) {
                    if (m.getKey().equals(word)) {
                        webEngine.loadContent(m.getValue());
                        meanw=m.getValue();
                        history_list.put(m.getKey(), m.getValue());
                        Write.Ouputfile(history_list, "history.txt");
                        break;
                    }
                    else {
                        String t = "<html>NO WORD<br>(add word to dictionary if you want)</html>";
                        webEngine.loadContent(t);
                        meanw="Từ bạn kiếm không có trong từ điển";
                    }
                }
            }
            listview.setVisible(false);
            listview.getItems().clear();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void suggestWords(KeyEvent keyevent){
        listview.setVisible(true);
        String give = input.getText();
        TreeMap<String, String> gest = new TreeMap<>();
        for (Map.Entry<String, String> sg : myList.entrySet()) {
            if (sg.getKey().startsWith(give)) {
                gest.put(sg.getKey(), sg.getValue());
            }
        }
        listview.getItems().clear();
        for (Map.Entry<String, String> k : gest.entrySet()) {
            control_View.add(k.getKey());
        }
        if(give.isEmpty() && (keyevent.getCode() == KeyCode.BACK_SPACE ||
                keyevent.getCode() == KeyCode.ESCAPE)){
            input.clear();
            listview.getItems().clear();
            listview.setVisible(false);
        }
    }


    //di chuyển bằng phinsm trên listview
    public void keyHangleOnListView(KeyEvent event) {
        input.setText(listview.getSelectionModel().getSelectedItem());
        WebEngine webEngine = mean.getEngine();
        String eng = input.getText();
        for (Map.Entry<String,String> focus : myList.entrySet()){
            if(focus.getKey().equals(eng)){
                webEngine.loadContent(focus.getValue());
            }
        }
    }


    public void clickStar(){
        String word = input.getText();
        String mean ="";
        for (Map.Entry<String,String> favo : myList.entrySet()){
            if(favo.getKey().equals(word)){
                mean = favo.getValue();
            }
        }
        if(!word.isEmpty()) {
            if (!favorite_list.containsKey(word) && myList.containsKey(word)) {
                favorite_list.put(word, mean);
                Alert succes = new Alert(Alert.AlertType.CONFIRMATION);
                Write.Ouputfile(favorite_list, "Favorite.txt");
                succes.setTitle("Congratulation");
                succes.setContentText("Đã thêm vào danh sách yêu thích");
                succes.show();
            } else {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setTitle("Error");
                error.setContentText("Từ đã có trong danh sách yêu thích của bạn hoặc từ bạn chọn không có trong từ điển");
                error.show();
            }
        }
        else {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Error");
            error.setContentText("Bạn chưa chọn từ");
            error.show();
        }
    }



    public void clickedOnListView(MouseEvent event) {
        try {
            WebEngine webEngine = mean.getEngine();
            input.setText(listview.getSelectionModel().getSelectedItem());
            String w = input.getText();
            for (Map.Entry<String, String> fx : myList.entrySet()) {
                if (fx.getKey().equals(w)) {
                    meanw=fx.getValue();
                    webEngine.loadContent(fx.getValue());
                }
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    //change to new stage for insert,remove and modify
    public void changeToNewScene(ActionEvent event,String path){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource(path));
            Parent newSceneAdd = loader.load();
            Scene insert = new Scene(newSceneAdd);
            Stage newWindow = (Stage)((Node)event.getSource()).getScene().getWindow();
            newWindow.setScene(insert);
            newWindow.show();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public void changeToNewScene1(ActionEvent event,String path){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource(path));
            Parent newSceneAdd = loader.load();
            Scene insert = new Scene(newSceneAdd);
            Stage newWindow = (Stage)root.getScene().getWindow();
            newWindow.setScene(insert);
            newWindow.show();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }


    //exit
    public void ExitOnMenuBar(ActionEvent event){
        Alert confim = new Alert(Alert.AlertType.CONFIRMATION);
        confim.setTitle("Bạn chọn");
        confim.setHeaderText("Thoát");
        confim.setContentText("Bạn chắc chắn muốn thoát chứ?");
        confim.showAndWait();
        if (confim.getResult() == ButtonType.OK){
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
        }

    }
    //add words
    public void gotoSceneAdd(ActionEvent event){ changeToNewScene1(event,"fileFxml/addmorewords.fxml"); }

    //delete words
    public void gotoSceneDelete(ActionEvent event){ changeToNewScene1(event,"fileFxml/deletewords.fxml"); }

    //modify words
    public void gotoSceneModify(ActionEvent event){ changeToNewScene1(event,"fileFxml/modifydictionary.fxml"); }

    //All words
    public void gotoSceneAll(ActionEvent event){ changeToNewScene1(event,"fileFxml/allword.fxml");}

    //favorite list
    public void gotoSceneFavorite(ActionEvent event){ changeToNewScene(event,"fileFxml/myfavorite.fxml"); }

    //history list
    public void gotoSceneHistory(ActionEvent event){changeToNewScene(event,"fileFxml/history.fxml");}


    //api google
    public void gotoSceceAPIGoogle(ActionEvent event) { changeToNewScene1(event,"fileFxml/apicontroller.fxml");}


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        favorite_list = Read.readFileDictionary("Favorite.txt");
        myList = Read.readFileDictionary("MyFile.txt");
        history_list = Read.readFileDictionary("history.txt");
        listview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listview.setItems(control_View);
        listview.setVisible(false);
        soundE.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            SoundGoogle.speak(input.getText(),"en");
        });
        soundV.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            SoundGoogle.speak(meanw,"vi");
        });
        star.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            clickStar();
        });
        Tooltip tooltip = new Tooltip("enter your words");
        tooltip.setAnchorLocation(PopupWindow.AnchorLocation.WINDOW_BOTTOM_LEFT);
        input.setTooltip(tooltip);
    }
}
